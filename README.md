CI/CD Workshop using Gitlab Pipelines
========================================

#### CI/CD simple setup:
1. Make a .gitlab-ci.yml file
2. Add the following stages Build, Test and Promote
3. Add a job in the Build stage where you execute: echo "Building the code..."
4. Add a job in the Test stage where you execute: echo "Testing the code..." 
5. Add a job in the Promote stage where you execute: echo "Building and promoting the code..." 
6. Commit your code and see your build

#### CI/CD with artifacts
1. Add the following to your first job: echo "Building complete." > test.txt
2. Now upload the test.txt as a artifact in your job
3. Add the following to your second job: cat test.txt
4. Commit your code and see your build. 


#### CI/CD building the real project
1. Make a build job for the angular app where 'npm install' is run in the build stage.
2. Make a promote job for the angular app where you build and push a docker image to the gitlab registry in the Promote stage.
3. Commit your code and see your build
4. Now make the promote steps manual
